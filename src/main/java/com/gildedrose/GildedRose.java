package com.gildedrose;

class GildedRose {
    private static final int MAX_QUALITY = 50;
    private static final int MIN_QUALITY = 0;
    Item[] items;
    public static final String AGED_BRIE = "Aged Brie";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String CONJURED = "Conjured";

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            switch (item.name) {
                case SULFURAS:
                    item.sellIn++;
                    break;
                case AGED_BRIE:
                        item.quality = qualityPlusOne(item.quality);
                    break;
                case BACKSTAGE_PASSES:
                    if (isItemExpired(item)) {
                        item.quality = MIN_QUALITY;
                    } else {
                        item.quality = qualityPlusOne(item.quality);
                        if (item.sellIn < 11) {
                            item.quality = qualityPlusOne(item.quality);
                        }
                        if (item.sellIn < 6) {
                            item.quality = qualityPlusOne(item.quality);
                        }
                    }
                    break;
                case CONJURED:
                    item.quality = reduceQuality(item);
                default:
                    item.quality = reduceQuality(item);
            }
            item.sellIn--;
        }
    }

    private int reduceQuality(Item item) {
        int quality = qualityMinusOne(item.quality);
        if (isItemExpired(item)) {
            quality = qualityMinusOne(quality);
        }
        return quality;
    }

    private Boolean isItemExpired(Item item) {
        return item.sellIn <= 0;
    }
    private int qualityPlusOne(int quality) {
        return quality < MAX_QUALITY ? quality + 1 : MAX_QUALITY;
    }
    private int qualityMinusOne(int quality) {
        return quality > MIN_QUALITY ? quality - 1 : MIN_QUALITY;
    }
}
