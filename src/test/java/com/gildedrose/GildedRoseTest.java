package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.GildedRose.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    private GildedRose app;
    private static String COMMON_ITEM = "foo";

    @Test
    void updateQuality_should_not_update_name() {
        Integer sellIn = 0;
        Integer quality = 0;
        setSingleItemsAndNewGildedRose(COMMON_ITEM, sellIn, quality);
        app.updateQuality();
        assertEquals(COMMON_ITEM, app.items[0].name);
    }

    @Test
    void updateQuality_should_decrease_sellIn_and_quality() {
        Integer sellIn = 1;
        Integer quality = 1;
        setSingleItemsAndNewGildedRose(COMMON_ITEM, sellIn, quality);
        app.updateQuality();
        Integer expected = 0;
        assertEquals(expected, app.items[0].quality);
        assertEquals(expected, app.items[0].sellIn);
    }

    @Test
    void when_sell_by_date_passes_quality_degrades_twice_as_fast() {
        Integer sellIn = 0;
        Integer quality = 10;
        setSingleItemsAndNewGildedRose(COMMON_ITEM, sellIn, quality);
        app.updateQuality();
        Integer expected = quality - 2;
        assertEquals(expected, app.items[0].quality);
    }

    @Test
    void quality_should_never_be_negative() {
        Integer sellIn = 0;
        Integer quality = 0;
        setSingleItemsAndNewGildedRose(COMMON_ITEM, sellIn, quality);
        app.updateQuality();
        Integer expected = 0;
        assertEquals(expected, app.items[0].quality);
        sellIn = 0;
        quality = 3;
        setSingleItemsAndNewGildedRose(CONJURED, sellIn, quality);
        app.updateQuality();
        assertEquals(expected, app.items[0].quality);
    }

    @Test
    void aged_brie_quality_should_increase() {
        Integer sellIn = 5;
        Integer quality = 5;
        setSingleItemsAndNewGildedRose(AGED_BRIE, sellIn, quality);
        app.updateQuality();
        Integer expectedSellIn = 4;
        Integer expectedQuality = 6;
        assertEquals(expectedSellIn, app.items[0].sellIn);
        assertEquals(expectedQuality, app.items[0].quality);
    }

    @Test
    void the_quality_of_an_item_should_never_be_more_than_50() {
        Integer sellIn = 5;
        Integer quality = 49;
        setSingleItemsAndNewGildedRose(AGED_BRIE, sellIn, quality);
        app.updateQuality();
        Integer expected = 50;
        assertEquals(expected, app.items[0].quality);
        app.updateQuality();
        assertEquals(expected, app.items[0].quality);
    }

    @Test
    void the_quality_of_sulfuras_should_never_change() {
        Integer sellIn = 1;
        Integer quality = 80;
        setSingleItemsAndNewGildedRose(SULFURAS, sellIn, quality);
        app.updateQuality();
        Integer expectedSellIn = 1;
        Integer expectedQuality = 80;
        assertEquals(expectedSellIn, app.items[0].sellIn);
        assertEquals(expectedQuality, app.items[0].quality);
        app.updateQuality();
        assertEquals(expectedSellIn, app.items[0].sellIn);
        assertEquals(expectedQuality, app.items[0].quality);
    }

    @Test
    void the_quality_backstage_passes_increase_by_2_for_sellIn_lesser_than_11() {
        Integer sellIn = 11;
        Integer quality = 30;
        setSingleItemsAndNewGildedRose(BACKSTAGE_PASSES, sellIn, quality);
        app.updateQuality();
        Integer expectedSellIn = 10;
        Integer expectedQuality = 31;
        assertEquals(expectedSellIn, app.items[0].sellIn);
        assertEquals(expectedQuality, app.items[0].quality);
        app.updateQuality();
        expectedQuality = 33;
        assertEquals(expectedQuality, app.items[0].quality);
    }

    @Test
    void the_quality_backstage_passes_increase_by_3_for_sellIn_lesser_than_6() {
        Integer sellIn = 6;
        Integer quality = 44;
        setSingleItemsAndNewGildedRose(BACKSTAGE_PASSES, sellIn, quality);
        app.updateQuality();
        Integer expected = 46;
        assertEquals(expected, app.items[0].quality);
        app.updateQuality();
        expected = 49;
        assertEquals(expected, app.items[0].quality);
        app.updateQuality();
        expected = 50;
        assertEquals(expected, app.items[0].quality);
        app.updateQuality();
        expected = 50;
        assertEquals(expected, app.items[0].quality);
    }

    @Test
    void the_quality_backstage_passes_decreases_to_0_after_date() {
        Integer sellIn = 0;
        Integer quality = 50;
        setSingleItemsAndNewGildedRose(BACKSTAGE_PASSES, sellIn, quality);
        app.updateQuality();
        Integer expected = 0;
        assertEquals(expected, app.items[0].quality);
        app.updateQuality();
        assertEquals(expected, app.items[0].quality);
    }

    @Test
    void the_application_should_work_for_multiple_items() {
        Integer sellIn = 0;
        Integer quality = 50;
        Item[] items = new Item[]{new Item(COMMON_ITEM, sellIn, quality), new Item(SULFURAS, sellIn, quality),
                new Item(BACKSTAGE_PASSES, sellIn, quality)};
        app = new GildedRose(items);
        app.updateQuality();
        Integer expectedSellIn = -1;
        Integer expectedQuality = 48;
        assertEquals(expectedSellIn, app.items[0].sellIn);
        assertEquals(expectedQuality, app.items[0].quality);
        expectedSellIn = 0;
        expectedQuality = 50;
        assertEquals(expectedSellIn, app.items[1].sellIn);
        assertEquals(expectedQuality, app.items[1].quality);
        expectedSellIn = -1;
        expectedQuality = 0;
        assertEquals(expectedSellIn, app.items[2].sellIn);
        assertEquals(expectedQuality, app.items[2].quality);
    }

    @Test
    void conjured_degrades_twice_as_fast() {
        Integer sellIn = 1;
        Integer quality = 10;
        setSingleItemsAndNewGildedRose(CONJURED, sellIn, quality);
        app.updateQuality();
        Integer expected = 8;
        assertEquals(expected, app.items[0].quality);
        app.updateQuality();
        expected = 4;
        assertEquals(expected, app.items[0].quality);
    }

    private void setSingleItemsAndNewGildedRose(String itemName, Integer sellIn, Integer quality) {
        Item[] items = new Item[]{new Item(itemName, sellIn, quality)};
        app = new GildedRose(items);
    }
}
