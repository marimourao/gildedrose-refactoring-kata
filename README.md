# Gilded Rose

Gilded Rose is a small inn located in Gent that also buys and sells goods. The goods are constantly degrading in quality as they approach their sell by date. 
This is a system that updates their inventory.
For more details about the application please read [GildedRoseRequirements.txt](https://bitbucket.org/marimourao/gildedrose-refactoring-kata/src/master/GildedRoseRequirements.txt) 

The following instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
## Prerequisites

The application will build on all major operating systems and requires only a **Java Development Kit** version 8 or higher. 
To check, run java -version. You should see something like this:

```
❯ java -version
java version "1.8.0_151"
Java(TM) SE Runtime Environment (build 1.8.0_151-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)
```
To download and install the JDK, access [Oracle](https://www.oracle.com/sa/java/technologies/javase-downloads.html) page.
For Windows users set the JAVA_HOME environment variable to point to the installation directory of the desired JDK and add $JAVA_HOME$\bin to PATH.

This application is built with **Gradle**, but you don’t need to install Gradle since this project has a Gradle Wrapper.
For more details visit [Gradle](https://docs.gradle.org/current/userguide/getting_started.html) page.

### Versioning
This project uses Git for versioning. BitBucket for code management.
Make sure you have git installed with the following command:
```
git --version
```
If git is installed, you will have an output like this:
```
git version 2.20.1 (Apple Git-117)
```
Otherwise follow the installation instructions in [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) website.

## Installing
To read and write to this project, the developer will need access permissions.
Contact [the candidate](https://www.linkedin.com/in/marismourao/) or any other Admin of this repository for the permission.

Please go to [Gilded Rose Repository](https://bitbucket.org/marimourao/gildedrose-refactoring-kata/src/master/). On the top right of the landing page click on "Clone".
The necessary command will be displayed, so copy that command and run it with your favorite shell from your workspace directory.

It will also give you the option to Clone in [Sourcetree](https://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_mac) if you prefer. 

### Building
This project can be opened from any IDE. From your favorite IDE you can select the option to open a file and open `build.gradle` as a project(so that your IDE already recognize it as a gradle project) or open the project folder and when adding a configuration, select Gradle Task.
##### Example for IntelliJ IDEA
**File** -> **Open...** -> _Navigate to the repository directory_ -> _Select_ `build.gradle` -> **Open as a Project**

#### Building it from IntelliJ IDEA
Once the project is opened, IntelliJ will automatically setup default configurations for a Gradle project. Make sure that the Java JDK version and folder is correctly selected. 
Accept all and wait a bit for the load. 

On the top right of IntelliJ you will find a place to configure the Run. Click on **Add/Edit Configurations...** , a window will open, select the `+` sign on the top left and add a new Gradle configuration.
Choose the name of your preference.  On **Gradle project:** fill or select the project: `gildedrose-refactoring-kata`, on **Tasks:** enter the wished task:
* _`assemble`_: to compile the project
* _`build`_: builds the project (this will also run the tests)
* _`check`_: runs the tests

Remaining configuration can be as default. Save the configuration and now it is possible to run the task with the >play< button and debug with the >bug< button.
#### Building it from a shell
To compile the code from your favorite Unix shell execute the following command from the project repository home:

```
sh gradlew assemble
```

You can build the application (tests included) with the following command:

```
sh gradlew build
```

If you wish to run only the tests, use:

```
sh gradlew check
```

From PowerShell instead of `sh` just execute `gradlew` like: ./gradlew

For more details and other gradle tasks visit [Gradle page](https://docs.gradle.org/current/userguide/getting_started.html)

## Contributing

This is a Kata originally created by Terry Hughes and improved by a vast number of contributors.
You can find the original code here: [GildedRose-Refactoring-Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata)

* **Mariana Mourao** - *Candidate Refactoring* - [Linkedin](https://www.linkedin.com/in/marismourao/)

See also the list of [contributors](https://github.com/emilybache/GildedRose-Refactoring-Kata/graphs/contributors) who participated in this project.

[comment]: TODO_Acknowledgments
[comment]: A_space_for_comments_tips_additional_information
##### The End.